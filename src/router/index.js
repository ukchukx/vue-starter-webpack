import Vue from 'vue';
import Router from 'vue-router';
import Hello from '@/components/Hello';
import { APP_CONSTANTS } from '@/constants';

Vue.use(Router);

const routes = [
  {
    path: '/home',
    name: APP_CONSTANTS.ROUTES.HOME,
    component: Hello,
    meta: {
      requiresAuth: false,
      groups: [
        APP_CONSTANTS.GROUPS.ADMIN,
        APP_CONSTANTS.GROUPS.USER
      ]
    }
  }
];

const router = new Router({
  routes,
  mode: 'history'
});

// Authenticate before accessing protected route
//  router.beforeEach((to, from, next) => {
//    if (to.matched.some(record => record.meta.requiresAuth)) {
//      const notAuthenticated = false;
//      const notPermitted = false;
//      if (notAuthenticated) { // No group? Send user to login
//        next({
//          name: ROUTE_CONSTANTS.SIGNIN,
//          params: { realm: to.params.realm },
//          query: { to: to.fullPath }
//        });
//      } // wrong group? return to sender
//      else if (to.meta.groups.indexOf(group) === -1) {
//        store.dispatch('logout');
//        next({ name: ROUTE_CONSTANTS.SIGNIN });
//      } else {
//        next();
//      }
//    } else {
//      next();
//    }
//  });

export default router;

