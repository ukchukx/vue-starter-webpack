const APP_CONSTANTS = {
  FAILED: 'failed',
  OK: 'ok',
  GROUPS: {
    ADMIN: 'admin',
    USER: 'user'
  },
  ROUTES: {
    HOME: 'home',
    SIGNIN: 'signin'
  }
};


const navMenu = [
  {
    name: 'Link #1',
    disabled: false,
    location: { name: APP_CONSTANTS.ROUTES.HOME },
    groups: [APP_CONSTANTS.GROUPS.ADMIN]
  },
  {
    name: 'Options',
    disabled: false,
    dropdown: [
      {
        name: 'Profile',
        location: { name: APP_CONSTANTS.ROUTES.HOME },
        groups: [
          APP_CONSTANTS.GROUPS.ADMIN,
          APP_CONSTANTS.GROUPS.USER
        ]
      },
      {
        name: 'Sign out',
        event: 'signout',
        groups: [
          APP_CONSTANTS.GROUPS.ADMIN,
          APP_CONSTANTS.GROUPS.USER
        ]
      }
    ],
    groups: [
      APP_CONSTANTS.GROUPS.ADMIN,
      APP_CONSTANTS.GROUPS.USER
    ]
  }
];

function makeUserMenu(group) {
  const menu = navMenu.filter((item) => {
    // eslint-disable-next-line no-prototype-builtins
    if (item.hasOwnProperty('dropdown')) { // Filter dropdown links
      item.dropdown = item.dropdown.filter(d => d.groups.indexOf(group) !== -1);
    }
    return item.groups.indexOf(group) !== -1;
  });

  return menu;
}

function getLogoLink(group) {
  switch (group) {
    case APP_CONSTANTS.GROUPS.ADMIN: return { name: APP_CONSTANTS.ROUTES.HOME };
    case APP_CONSTANTS.GROUPS.USER: return { name: APP_CONSTANTS.ROUTES.HOME };
    default: return '#';
  }
}

function setUpEnvironment(store) {
  store.dispatch('fetchUsers');
}

export {
  APP_CONSTANTS,
  makeUserMenu,
  getLogoLink,
  setUpEnvironment
};

