import Vue from 'vue';
import Vuex from 'vuex';
import VueResource from 'vue-resource';
import createPersistedState from 'vuex-persistedstate';
import createMutationsSharer from 'vuex-shared-mutations';

import users from './modules/users';

Vue.use(Vuex);
Vue.use(VueResource);

/* eslint-disable no-undef */
Vue.http.options.root = `http://${window.location.host}/api/v1`;
Vue.http.interceptors.push((request, next) => {
  request.headers.set('Authorization', `Bearer ${store.getters.getUser.token}`);
  request.headers.set('Access-Control-Allow-Origin', `http://${window.location.host}`);
  request.headers.set('Access-Control-Allow-Methods', '*');
  request.headers.set('Access-Control-Expose-Headers', 'Authorization,Location,Content-Type');
  next();
});

const debug = process.env.NODE_ENV !== 'production';

export default new Vuex.Store({
  modules: {
    users
  },
  strict: debug,
  plugins: [
    createMutationsSharer({ predicate: (mutation, state) => true }),
    createPersistedState({ key: 'vue-starter' })
  ]
});
