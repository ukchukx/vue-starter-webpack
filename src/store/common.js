import Vue from 'vue';
import { APP_CONSTANTS } from '@/constants';

/* eslint-disable no-console */

function fetch(commit, api, type) {
  const msg = `[GET] ${api}`;
  Vue.http.get(`${api}`)
    .then((response) => {
      commit(type, response.body.data);
      console.log(`${msg} returned ${response.body.data.length} results.`);
    }, (error) => {
      console.log(`${type} error`, error.body);
    });
}

function doDelete(commit, id, api, type) {
  console.log(`[DELETE] ${api}/${id}`);
  return Vue.http.delete(`${api}/${id}`)
    .then((response) => {
      commit(type, id);
      return APP_CONSTANTS.OK;
    }, (error) => {
      console.log(`${type} error`, error.body);
      return APP_CONSTANTS.FAILED;
    });
}

function save(commit, obj, api, type) {
  if (obj.id !== 0) {
    console.log(`[PUT] ${api}/${obj.id}`);
    return Vue.http.put(`${api}/${obj.id}`, obj)
      .then((response) => {
        commit(type, response.body);
        return APP_CONSTANTS.SAVED;
      }, (error) => {
        console.log(`${type} error`, error.body);
        return APP_CONSTANTS.FAILED;
      });
  }
  console.log(`[POST] ${api}`);
  return Vue.http.post(`${api}`, obj)
    .then((response) => {
      const idParts = response.headers.get('Location').split('/');
      obj.id = idParts[idParts.length - 1];
      commit(type, obj);
      return APP_CONSTANTS.SAVED;
    }, (error) => {
      console.log(`${type} error`, error.body);
      return APP_CONSTANTS.FAILED;
    });
}

export { fetch, save, doDelete };
