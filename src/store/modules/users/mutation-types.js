export const FETCH_USERS = 'users/fetch_users';
export const SAVE_USER = 'users/save_user';
export const DELETE_USER = 'users/delete_user';
export const REMOVE_AUTH_USER = 'users/remove_auth_user';
export const SET_AUTH_USER = 'users/set_auth_user';
