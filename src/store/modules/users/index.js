import * as actions from './actions';
import * as getters from './getters';

import {
  FETCH_USERS,
  SAVE_USER,
  DELETE_USER,
  REMOVE_AUTH_USER,
  SET_AUTH_USER
} from './mutation-types';

const initialState = {
  users: [],
  user: {}
};

/* eslint-disable no-param-reassign */
const mutations = {
  [FETCH_USERS](state, users) {
    state.users = users;
  },
  [SAVE_USER](state, user) {
    const index = state.users.findIndex(u => u.id === user.id);

    if (index !== -1) {
      // We need to replace the array entirely so that vue can recognize
      // the change and re-render entirely.
      // See http://vuejs.org/guide/list.html#Caveats
      state.users.splice(index, 1, user);
    } else {
      state.users.push(user);
    }
  },
  [DELETE_USER](state, userId) {
    state.users = state.users.filter(u => u.id !== userId);
  },
  [REMOVE_AUTH_USER](state) {
    state.user = {};
  },
  [SET_AUTH_USER](state, details) {
    state.user = Object.assign(state.user, details);
  }
};

export default {
  state: Object.assign({}, initialState),
  actions,
  getters,
  mutations
};
