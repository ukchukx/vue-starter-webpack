import Vue from 'vue';
import { save, fetch, doDelete } from '@/store/common';
import { APP_CONSTANTS } from '@/constants';

import {
  FETCH_USERS,
  DELETE_USER,
  SAVE_USER,
  SET_AUTH_USER,
  REMOVE_AUTH_USER
} from './mutation-types';

const userApi = 'users';
const authApi = 'auth';


/* eslint-disable no-unused-vars */
/* eslint-disable no-console */

export function fetchUsers({ commit }) {
  return fetch(commit, userApi, FETCH_USERS);
}

export function saveUser({ commit }, user) {
  return save(commit, user, userApi, SAVE_USER);
}

export function deleteUser({ commit }, userId) {
  return doDelete(commit, userId, userApi, DELETE_USER);
}

export function authenticate({ commit }, details) {
  return Vue.http.post(`${authApi}/authenticate`, details)
  .then((response) => {
    response.body.password = details.password;
    commit(SET_AUTH_USER, response.body);
    return APP_CONSTANTS.OK;
  }, (error) => {
    console.log(`${SET_AUTH_USER}`, error); // how to handle api errors?
    return APP_CONSTANTS.FAILED;
  });
}

export function logout({ commit }) {
  commit(REMOVE_AUTH_USER);
}

export function updateUserDetails({ commit }, details) {
  commit(SET_AUTH_USER, details);
  return APP_CONSTANTS.OK;
}

